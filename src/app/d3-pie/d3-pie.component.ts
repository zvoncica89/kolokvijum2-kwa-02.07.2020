import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";

import * as d3 from "d3";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-d3-pie",
  templateUrl: "./d3-pie.component.html",
  styleUrls: ["./d3-pie.component.css"],
})
export class D3PieComponent implements OnInit {
  @ViewChild("chart") private chartContainer: ElementRef;
  private users;
  private dataSet = {};
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getAllUsers();
    // this.renderPiechart();
    // this.generateSecondPie();
  }

  getDataSet(users) {
    for (let u of users) {
      if (!this.dataSet[u.roles]) {
        let dodaj = {};

        this.dataSet[u.roles] = 1;
      } else {
        this.dataSet[u.roles] += 1;
      }
    }
    console.log(this.dataSet);
    this.generateSecondPie();
  }
  getAllUsers() {
    this.http.get("http://localhost:8083/api/users").subscribe((r) => {
      console.log(this.users);
      this.users = r;
      this.getDataSet(this.users);
    });
  }

  renderPiechart() {
    //   // let dataset = [
    //   //   { label: "red", count: 10 },
    //   //   { label: "Betelgeuse", count: 20 },
    //   //   { label: "Cantaloupe", count: 30 },
    //   //   { label: "Dijkstra", count: 40 },
    //   // ];

    //   let width: number = 360;
    //   let height: number = 360;
    //   let radius: number = Math.min(width, height) / 2;

    //   let color: any = d3.scaleOrdinal().range(d3.schemeCategory20b);

    //   let element: any = this.chartContainer.nativeElement;

    //   // Create element
    //   let svg = d3
    //     .select(element)
    //     .append("svg")
    //     .attr("width", width)
    //     .attr("height", height)
    //     .append("g")
    //     .attr("transform", `translate(${width / 2}, ${height / 2})`);

    //   // Define the radius
    //   let arc = d3.arc().innerRadius(0).outerRadius(radius);

    //   // Define start and end angles of the segments
    //   let pie = d3
    //     .pie()
    //     .value(function (d) {
    //       return d.count;
    //     })
    //     .sort(null);

    //   // Render the chart
    //   let path = svg
    //     .selectAll("path")
    //     // .data(pie(dataset))
    //     .enter()
    //     .append("path")
    //     .attr("d", arc)
    //     .attr("fill", function (d, i) {
    //       return color(d.data.label);
    //     });
    var data = [2, 4, 8, 10];

    var svg = d3.select("svg"),
      width = svg.attr("width"),
      height = svg.attr("height"),
      radius = Math.min(width, height) / 2,
      g = svg
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var color = d3.scaleOrdinal([
      "#4daf4a",
      "#377eb8",
      "#ff7f00",
      "#984ea3",
      "#e41a1c",
    ]);

    // Generate the pie
    var pie = d3.pie();

    // Generate the arcs
    var arc = d3.arc().innerRadius(0).outerRadius(radius);

    //Generate groups
    var arcs = g
      .selectAll("arc")
      .data(pie(data))
      .enter()
      .append("g")
      .attr("class", "arc");

    //Draw arc paths
    arcs
      .append("path")
      .attr("fill", function (d, i) {
        return color(i);
      })
      .attr("d", arc);
  }

  generateSecondPie() {
    // set the dimensions and margins of the graph
    var width = 450;
    var height = 450;
    var margin = 40;

    // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
    var radius = Math.min(width, height) / 2 - margin;

    // append the svg object to the div called 'my_dataviz'
    var svg = d3
      .select("#my_dataviz")
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    // Create dummy data
    // var data = { a: 9, b: 20, c: 30, d: 8, e: 12 };

    // set the color scale
    var color = d3.scaleOrdinal().domain(this.dataSet).range(d3.schemeSet2);

    // Compute the position of each group on the pie:
    var pie = d3.pie().value(function (d) {
      return d.value;
    });
    var data_ready = pie(d3.entries(this.dataSet));
    // Now I know that group A goes from 0 degrees to x degrees and so on.

    // shape helper to build arcs:
    var arcGenerator = d3.arc().innerRadius(0).outerRadius(radius);

    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    svg
      .selectAll("mySlices")
      .data(data_ready)
      .enter()
      .append("path")
      .attr("d", arcGenerator)
      .attr("fill", function (d) {
        return color(d.data.key);
      })
      .attr("stroke", "black")
      .style("stroke-width", "2px")
      .style("opacity", 0.7);

    // Now add the annotation. Use the centroid method to get the best coordinates
    svg
      .selectAll("mySlices")
      .data(data_ready)
      .enter()
      .append("text")
      .text(function (d) {
        return "grp " + d.data.key;
      })
      .attr("transform", function (d) {
        return "translate(" + arcGenerator.centroid(d) + ")";
      })
      .style("text-anchor", "middle")
      .style("font-size", 17);
  }
}
