import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { UsersComponent } from "./users/users.component";
import { RegistracijaComponent } from "./registracija/registracija.component";
import { IzmenaComponent } from "./izmena/izmena.component";
import { DetaljiComponent } from "./detalji/detalji.component";
import { RolesComponent } from "./roles/roles.component";
import { DodavanjeRoleComponent } from "./dodavanje-role/dodavanje-role.component";
import { IzmenaRolaComponent } from "./izmena-rola/izmena-rola.component";
import { D3PieComponent } from "./d3-pie/d3-pie.component";

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "users",
    component: UsersComponent,
  },
  {
    path: "registracija",
    component: RegistracijaComponent,
  },
  {
    path: "user/:id",
    component: IzmenaComponent,
  },
  {
    path: "users/:id",
    component: DetaljiComponent,
  },
  {
    path: "roles",
    component: RolesComponent,
  },
  {
    path: "novaRola",
    component: DodavanjeRoleComponent,
  },
  {
    path: "izmenaRole/:id",
    component: IzmenaRolaComponent,
  },
  {
    path: "d3",
    component: D3PieComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
