import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "../login.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Role } from "../model/Role";
import { NgForOf } from "@angular/common";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-izmena-rola",
  templateUrl: "./izmena-rola.component.html",
  styleUrls: ["./izmena-rola.component.css"],
})
export class IzmenaRolaComponent implements OnInit {
  public roles: any = [];
  public rolaIzmena: Role = {
    id: null,
    title: "",
    role: "",
  };
  constructor(
    private http: HttpClient,
    public loginService: LoginService,
    private router: Router,
    private aRoute: ActivatedRoute
  ) {}

  getAllRoles() {
    this.http.get("http://localhost:8083/api/roles").subscribe((r) => {
      console.log(this.roles);
      this.roles = r;
    });
  }
  getRola() {
    console.log(this.aRoute.snapshot.params.id);
    this.http
      .get("http://localhost:8083/api/roles/" + this.aRoute.snapshot.params.id)
      .subscribe(
        (rola: Role) => {
          this.rolaIzmena = rola;
          console.log(rola);
        },
        (err) => console.log(err)
      );
  }
  ngOnInit(): void {
    this.getAllRoles();
    this.getRola();
  }
  submit(form: NgForm) {
    this.rolaIzmena.role = "ROLE_" + this.rolaIzmena.title.toLocaleUpperCase();
    this.http
      .put(
        "http://localhost:8083/api/roles/" + this.rolaIzmena.id,
        this.rolaIzmena
      )
      .subscribe(
        (rola: Role) => {
          window.alert("Uspesno izmenjena rola!");
          this.router.navigate(["/roles"]);
        },
        (err) => console.log(err)
      );
  }
}
