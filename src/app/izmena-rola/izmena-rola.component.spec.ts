import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IzmenaRolaComponent } from './izmena-rola.component';

describe('IzmenaRolaComponent', () => {
  let component: IzmenaRolaComponent;
  let fixture: ComponentFixture<IzmenaRolaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IzmenaRolaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IzmenaRolaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
