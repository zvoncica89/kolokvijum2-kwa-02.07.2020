import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "../login.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Role } from "../model/Role";

@Component({
  selector: "app-dodavanje-role",
  templateUrl: "./dodavanje-role.component.html",
  styleUrls: ["./dodavanje-role.component.css"],
})
export class DodavanjeRoleComponent implements OnInit {
  public novaRola: Role = {
    id: null,
    title: "",
    role: "",
  };
  constructor(
    public loginService: LoginService,
    public http: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {}
  submit(form: NgForm) {
    this.novaRola.role = "ROLE_" + this.novaRola.title.toLocaleUpperCase();
    this.http.post("http://localhost:8083/api/roles", this.novaRola).subscribe(
      (r: Role) => {
        window.alert("Uspesno kreirana nova rola");
        this.router.navigate(["/roles"]);
      },
      (err) => console.log("Neuspesno")
    );
  }
}
