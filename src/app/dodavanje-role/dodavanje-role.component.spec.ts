import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DodavanjeRoleComponent } from './dodavanje-role.component';

describe('DodavanjeRoleComponent', () => {
  let component: DodavanjeRoleComponent;
  let fixture: ComponentFixture<DodavanjeRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DodavanjeRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DodavanjeRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
