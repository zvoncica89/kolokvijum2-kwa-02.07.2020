export interface Role {
  id: number;
  title: string;
  role: string;
}
