import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";
import { User } from "../model/user";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  user = {
    username: null,
    password: null,
  };

  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit(): void {}

  login() {
    this.loginService.login(this.user).subscribe(
      (r) => {
        this.router.navigate(["users"]);
        console.log(r);
      },
      (r) => {
        console.log("failed");
      }
    );
  }
}
