import { Component, OnInit } from "@angular/core";
import { User } from "../model/user";
import { HttpClient } from "@angular/common/http";
import { LoginService } from "../login.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";

@Component({
  selector: "app-izmena",
  templateUrl: "./izmena.component.html",
  styleUrls: ["./izmena.component.css"],
})
export class IzmenaComponent implements OnInit {
  public korisnik: User = {
    id: null,
    username: "",
    password: "",
    roles: [],
  };
  public roles: any = [];
  constructor(
    private http: HttpClient,
    public loginService: LoginService,
    private router: Router,
    private aRoute: ActivatedRoute
  ) {}

  getAllRoles() {
    this.http.get("http://localhost:8083/api/roles").subscribe((r) => {
      console.log(this.roles);
      this.roles = r;
    });
  }
  getKorisnik() {
    console.log(this.aRoute.snapshot.params.id);
    this.http
      .get("http://localhost:8083/api/users/" + this.aRoute.snapshot.params.id)
      .subscribe(
        (korisnik: User) => {
          this.korisnik = korisnik;
          console.log(korisnik);
        },
        (err) => console.log(err)
      );
  }

  ngOnInit(): void {
    this.getAllRoles();
    this.getKorisnik();
  }

  submit(form: NgForm) {
    this.http
      .put("http://localhost:8083/api/users/" + this.korisnik.id, this.korisnik)
      .subscribe(
        (user: User) => {
          window.alert("Uspesno izmenjen korisnik!");
          this.router.navigate(["/users"]);
        },
        (err) => console.log(err)
      );
  }
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
  // drop(event: CdkDragDrop<string[]>) {
  //   if (event.previousContainer === event.container) {
  //     moveItemInArray(
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   } else {
  //     transferArrayItem(
  //       event.previousContainer.data,
  //       event.container.data,
  //       event.previousIndex,
  //       event.currentIndex
  //     );
  //   }
  // }
}
